package com.example.bmi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.example.bmi.databinding.ActivityMainBinding
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.bmiResult.text = "BMI : - "
        binding.calculateButton.setOnClickListener { calculateBMI() }
        binding.bodyHeightEditText.setOnKeyListener { view, keyCode, _ ->
            handleKeyEvent(
                view,
                keyCode
            )
        }
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

    private fun calculateBMI() {
        val weightOfBody = binding.bodyWeightEditText.text.toString().toDoubleOrNull()

        val heightOfBody = binding.bodyHeightEditText.text.toString().toDoubleOrNull()

        val bmiImage: ImageView = findViewById(R.id.imageView)

        if (weightOfBody == null || heightOfBody == null
            || weightOfBody == 0.0 || heightOfBody == 0.0
        ){
            binding.bmiResult.text = "BMI : ???"
            return
        }
        val heightMeter = heightOfBody/100
        var bmi = weightOfBody / (heightMeter * heightMeter)
        var drawableResource = when{
            bmi < 18.5 -> R.drawable.under
            bmi in 18.5..22.9 -> R.drawable.normal
            bmi in 23.0..24.9 -> R.drawable.risk
            bmi in 25.0..29.9 -> R.drawable.over
            else -> R.drawable.obese
        }

        bmiImage.setImageResource(drawableResource)
        val df = DecimalFormat("#.#")
        var formatBMI = df.format(bmi)
        //val result = NumberFormat.getNumberInstance().format(formatBMI)
        binding.bmiResult.text = getString(R.string.bmi_s, formatBMI)
    }
}